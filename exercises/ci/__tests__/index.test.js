const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

const pathToFixture = path.join(__dirname, '../__fixtures__/package.json');
const getCurrentVersion = (pathToFile) => {
  const currentVersion = fs.readFileSync(pathToFile);
  const versionObject = JSON.parse(currentVersion);
  return versionObject.version;
};

describe('Function "upVersion"', () => {
  afterEach(() => {
    const originalVersion = '{"version":"1.3.2"}';
    fs.writeFileSync(pathToFixture, originalVersion);
  });

  test('Key "major" should update major version', () => {
    const expected = '2.0.0';
    upVersion(pathToFixture, 'major');
    const actual = getCurrentVersion(pathToFixture);
    expect(actual).toEqual(expected);
  });

  test('Key "minor" should update minor version', () => {
    const expected = '1.4.0';
    upVersion(pathToFixture, 'minor');
    const actual = getCurrentVersion(pathToFixture);
    expect(actual).toEqual(expected);
  });

  test('Key "patch" should update patch version', () => {
    const expected = '1.3.3';
    upVersion(pathToFixture, 'patch');
    const actual = getCurrentVersion(pathToFixture);
    expect(actual).toEqual(expected);
  });

  test('Run without key should update patch version', () => {
    const expected = '1.3.3';
    upVersion(pathToFixture);
    const actual = getCurrentVersion(pathToFixture);
    expect(actual).toEqual(expected);
  });

  test('Run with incorrect key should not update any version', () => {
    const expected = '1.3.2';
    upVersion(pathToFixture, 'version');
    const actual = getCurrentVersion(pathToFixture);
    expect(actual).toEqual(expected);
  });

  test('Run without arguments should return error', () => {
    expect(() => upVersion()).toThrow('The "path" argument must be of type string or an instance of Buffer or URL');
  });
});
