const assert = require('power-assert');
const { flattenDepth } = require('lodash');

const array = [1, [2, [3, [4]], 5]];

assert.deepEqual(flattenDepth([]), [], 'Run with empty array');

assert.deepEqual(flattenDepth(), [], 'Run without array');

assert.deepEqual(flattenDepth(array), [1, 2, [3, [4]], 5], 'Default run');

assert.deepEqual(flattenDepth(array, 1), [1, 2, [3, [4]], 5], 'Run with 1 recursion depth');

assert.deepEqual(flattenDepth(array, 2), [1, 2, 3, [4], 5], 'Run with 2 recursion depth');

assert.deepEqual(flattenDepth(array, 3), [1, 2, 3, 4, 5], 'Run with 3 recursion depth');

assert.deepEqual(flattenDepth(array, 4), [1, 2, 3, 4, 5], 'Run with 4 recursion depth');

assert.deepEqual(flattenDepth(array, 0), array, 'Run with 0 recursion depth');

assert.deepEqual(flattenDepth(array, -1), array, 'Run with -1 recursion depth');

assert.deepEqual(flattenDepth(array, false), array, 'Run with "false" second argument');

assert.deepEqual(flattenDepth(array, true), [1, 2, [3, [4]], 5], 'Run with "true" second argument');

assert.deepEqual(flattenDepth(array, '3'), [1, 2, 3, 4, 5], 'Run with string second argument');
