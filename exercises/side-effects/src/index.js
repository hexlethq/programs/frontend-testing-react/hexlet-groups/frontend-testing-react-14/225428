const fs = require('fs');

const updateVersion = (currentVersion, keyWord) => {
  const [major, minor, patch] = currentVersion.split('.').map((element) => Number(element));
  if (keyWord === 'major') {
    return `${major + 1}.0.0`;
  }

  if (keyWord === 'minor') {
    return `${major}.${minor + 1}.0`;
  }

  if (keyWord === 'patch') {
    return `${major}.${minor}.${patch + 1}`;
  }

  return `${major}.${minor}.${patch}`;
};

const upVersion = (path, keyWord = 'patch') => {
  const data = fs.readFileSync(path, 'UTF-8');
  const packageObject = JSON.parse(data);
  const newWersion = updateVersion(packageObject.version, keyWord);
  packageObject.version = newWersion;

  return fs.writeFileSync(path, JSON.stringify(packageObject), 'UTF-8');
};

module.exports = { upVersion };
