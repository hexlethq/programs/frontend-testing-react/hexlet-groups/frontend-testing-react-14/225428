const fc = require('fast-check');
const _ = require('lodash');

const sort = (data) => data.slice().sort((a, b) => a - b);

describe('Testing sort function', () => {
  test('Array has sorted in ascending order', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer()), (data) => {
          const sortedData = sort(data);
          expect(sortedData).toBeSorted();
        },
      ),
    );
  });

  test('Sorting function return new array', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer()), (data) => {
          const sortedData = sort(data);
          expect(sortedData).not.toBe(data);
        },
      ),
    );
  });

  test('Array has the same length after sorting', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer()), (data) => {
          const sortedData = sort(data);
          expect(sortedData).toHaveLength(data.length);
        },
      ),
    );
  });

  test('Array has the same elements after sorting', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer()), (data) => {
          const sortedData = sort(data);
          const difference = _.difference(data, sortedData);
          expect(difference).toEqual([]);
        },
      ),
    );
  });

  test('Sorting of already sorted array doesn`t change elements order', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer()), (data) => {
          const firstSortedData = sort(data);
          const secondSortedData = sort(firstSortedData);
          expect(secondSortedData).toEqual(firstSortedData);
        },
      ),
    );
  });

  test('Run function without data return the error', () => {
    expect(() => sort()).toThrow('Cannot read property \'slice\' of undefined');
  });
});
