const faker = require('faker');

describe('Faker "createTransaction" helper', () => {
  test('Should return object', () => {
    const transaction = faker.helpers.createTransaction();
    expect(transaction).toBeInstanceOf(Object);
  });

  test('Has the same length', () => {
    const transaction = faker.helpers.createTransaction();
    expect(Object.keys(transaction)).toHaveLength(6);
  });

  test('Has expected data types', () => {
    const transaction = faker.helpers.createTransaction();
    expect(transaction).toEqual({
      amount: expect.stringMatching(/\d+/g),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.stringMatching(/\d+/g),
    });
  });

  test('Should return random result', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();
    expect(transaction1).not.toBe(transaction2);
    expect(transaction1).not.toMatchObject(transaction2);
  });
});
