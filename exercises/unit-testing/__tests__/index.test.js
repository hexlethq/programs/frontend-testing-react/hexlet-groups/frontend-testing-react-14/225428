/* eslint-disable prefer-object-spread */
describe('"Object.assign" testing', () => {
  test('Merging empty objects', () => {
    const result = Object.assign({}, {});
    expect(result).toEqual({});
  });

  test('Copying object', () => {
    const object1 = { a: 1, b: 2 };
    const object2 = { c: 3, d: 4 };
    const result1 = Object.assign({}, object1);
    const result2 = Object.assign(object2, {});

    expect(result1).toEqual({ a: 1, b: 2 });
    expect(result2).toEqual({ c: 3, d: 4 });
  });

  test('Merging two objects', () => {
    const object1 = { a: 1, b: 2 };
    const object2 = { c: 3, d: 4, b: 5 };
    const object3 = { c: 'string', d: null, b: 5 };
    const result1 = Object.assign(object1, object2);
    const result2 = Object.assign(object2, object3);

    expect(result1).toEqual({
      a: 1, b: 5, c: 3, d: 4,
    });
    expect(result1).toBe(object1);
    expect(result2).toEqual({ c: 'string', d: null, b: 5 });
    expect(result2).toBe(object2);
  });

  test('Merging three objects', () => {
    const object1 = { a: 1, b: 2 };
    const object2 = { c: 3, d: 4, b: 5 };
    const object3 = { c: 'string', d: null, b: 6 };
    const result = Object.assign(object1, object2, object3);

    expect(result).toEqual({
      a: 1, b: 6, c: 'string', d: null,
    });
    expect(result).toBe(object1);
  });

  test('Deep merging and cloning', () => {
    const object1 = { a: 1, b: { c: 2 } };
    const object2 = { a: 10, b: { c: 20 } };
    const result = Object.assign(object1, object2);

    expect(result).toEqual({ a: 10, b: { c: 20 } });
    expect(result).toBe(object1);
  });

  test('Merge objects with not writable properties', () => {
    const object1 = Object.defineProperty({ a: 1, b: 2, c: 3 }, 'd', {
      value: 4,
      writable: false,
    });
    const object2 = { d: 5 };
    expect(() => {
      Object.assign(object1, object2);
    }).toThrow('Cannot assign to read only property \'d\' of object');
  });
});
