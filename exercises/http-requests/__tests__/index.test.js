const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');

const baseURL = 'https://httpbin.org';
const data = {
  firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33,
};

describe('GET request testing', () => {
  beforeAll(() => nock.disableNetConnect());
  afterEach(() => nock.cleanAll());
  afterAll(() => nock.enableNetConnect());

  test('Should return 200 HTTP status code', async () => {
    const expected = data;
    const isDone = nock(baseURL)
      .get('/anything')
      .reply(200, expected)
      .isDone();
    const actual = await get(`${baseURL}/anything`);
    expect(expected).toEqual(actual);
    expect(isDone).toBeFalsy();
  });

  test('Should return 400 HTTP status code', async () => {
    const isDone = nock(baseURL)
      .get('/anything')
      .reply(400)
      .isDone();
    await expect(get(`${baseURL}/anything`)).rejects.toThrow();
    expect(isDone).toBeFalsy();
  });

  test('Should return 500 HTTP status code', async () => {
    const isDone = nock(baseURL)
      .get('/anything')
      .reply(400)
      .isDone();
    await expect(get(`${baseURL}/anything`)).rejects.toThrow();
    expect(isDone).toBeFalsy();
  });
});

describe('POST request testing', () => {
  test('Should return 200 HTTP status code', async () => {
    const expected = {
      status: 'ok',
    };
    const isDone = nock(baseURL)
      .post('/anything', data)
      .reply(200, expected)
      .isDone();
    const actual = await post(`${baseURL}/anything`, data);
    expect(expected).toEqual(actual);
    expect(isDone).toBeFalsy();
  });

  test('Should return 400 HTTP status code', async () => {
    const isDone = nock(baseURL)
      .post('/anything', data)
      .reply(400)
      .isDone();
    await expect(post(`${baseURL}/anything`)).rejects.toThrow();
    expect(isDone).toBeFalsy();
  });

  test('Should return 500 HTTP status code', async () => {
    const isDone = nock(baseURL)
      .post('/anything', data)
      .reply(400)
      .isDone();
    await expect(post(`${baseURL}/anything`)).rejects.toThrow();
    expect(isDone).toBeFalsy();
  });
});
